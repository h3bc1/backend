# Teste de Admissão
O Supermercado Zézinho precisa de um novo sistema para gerir o seu estoque de produtos. Seu papel nesse projeto é desenvolver uma API RESTful que forneça endpoints para gerenciar o cadastro dos produtos e fornecedores.

**INSTRUÇÕES**
- Primeiramente, você deve fazer um fork deste repositório, e versionar o seu código nele;
- Forneça instruções em um arquivo .MD sobre como montar a sua aplicação, mencionando cada etapa necessária para que consigamos colocá-la para rodar. Especifique qual banco de dados você usou, e qual versão;
- Quando terminar, você deve entregar o código fazendo um merge request. Atenção, pois uma vez submetido o request, o seu teste estará finalizado e não será mais possível voltar atrás;
- Iremos avaliar todos os aspectos do seu trabalho, tais como a forma como organiza o seu código, a sua lógica de programação e orientação a objetos, quantos diferentes paradigmas de programação você conhece e aplicou no teste, e até mesmo a forma como descreve os seus commits;
- Você deve usar o .NET Core versão 2 ou superior

## 2 - Requisitos de Negócio - Produtos
O cadastro do produto deve fornecer os seguintes atributos e operações.

### 2.1 - Atributos do Produto
Um produto possui os seguintes atributos:

- Id
- Descrição
- Preço em R$
- Quantidade em estoque
- Fornecedor

### 2.2 - Cadastrar um Produto
Forneça um endpoint que permita cadastrar um novo produto. Algumas regras de validação devem ser aplicadas:

- Descrição
    - Obrigatório
    - Mínimo 10 caracteres
    - Máximo 300 caracteres
- Preço em R$
    - Obrigatório
- Quantidade em estoque
    - Obrigatório
    - Mínimo 1
- Fornecedor
    - Obrigatório

### 2.3 - Deletar um Produto
Forneça um endpoint que permita deletar um produto por id.

### 2.4 - Alterar um Produto
Forneça um endpoint que permita alterar um produto por id. Algumas regras de validação se aplicam:

- Id
    - Não pode ser alterado
- Descrição
    - Obrigatório
    - Mínimo 10 caracteres
    - Máximo 300 caracteres
- Preço em R$
    - Obrigatório
- Quantidade em estoque
    - Obrigatório
    - Mínimo 1
- Fornecedor
    - Obrigatório

### 2.5 - Ler um Produto
Forneça um endpoint que permita oter os detalhes de um produto por id. Deve trazer todos os dados do produto.

### 2.6 - Listar Produtos
Forneça um endpoint que liste todos os produtos, trazendo todos os seus atributos. Além disso, esse endpoint deve conter alguns filtros, que permitem refinar os resultados. Esses filtros podem ser combinados. São eles:

- Id
- Descrição
- Fornecedor
- Preço mínimo
- Preço máximo

## 3 - Requisitos de Negócio - Fornecedores
O cadastro dos fornecedores deve conter os seguintes atributos e operações.

### 3.1 - Atributos do Fornecedor
Um fornecedor possui os seguintes atributos:

- Id
- Nome

### 3.2 - Cadastrar Fornecedor
Forneça um endpoint que permita cadastrar um fornecedor. As seguintes regras de validação se aplicam:

- Nome
    - Obrigatório
    - Mínimo 10 caracteres
    - Máximo 50 caracteres

### 3.3 - Alterar um Fornecedor
Forneça um endpoint que permita editar um fornecedor. As seguintes regras se aplicam:

- Id
    - Não pode ser alterado
- Nome
    - Obrigatório
    - Mínimo 10 caracteres
    - Máximo 50 caracteres

### 3.4 - Deletar um Fornecedor
Forneça um endpoint que permita deletar um fornecedor. As seguintes regras se aplicam:

- O fornecedor não pode ser deletado caso ele possua algum produto cadastrado.

### 3.5 - Ler um Fornecedor
Forneça um endpoint que permita visualizar os detalhes do fornecedor. Ele deve exibir os seguintes detalhes:

- Id
- Nome
- Lista de todos os produtos desse fornecedor

### 3.6 - Listar fornecedores
Forneça um endpoint que permita listar os fornecedores, mostrando o nome e o id de cada fornecedor.
